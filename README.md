# This repository moved.#
Please use the new location: https://github.com/mintforpeople/robobo-hri-emotion

# ROBOBO Emotion Library #

This repository contains the source code of the different modules that made up the ROBOBO Emotion Library. 

This includes:

- Faces module.
- Emotion sounds module.


NOTE: All the development happens in the develop or feature branches. The master branch contains the last stable release.